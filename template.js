  function displayNextImage() {
              x = (x === images.length - 1) ? 0 : x + 1;
              document.getElementById("img").src = images[x];
          }

          function displayPreviousImage() {
              x = (x <= 0) ? images.length - 1 : x - 1;
              document.getElementById("img").src = images[x];
          }

          function startTimer() {
              setInterval(displayNextImage, 3000);
          }

          var images = [], x = -1;
          images[0] = "la-jolla-shores-beach-san-diego-california-GOSANDIEGO0519[1].jpg"; 
          images[1] = "image2.jpg";
          images[2] = "image3.jpg";